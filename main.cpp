#include <iostream>

using namespace		std;

template<class T>
class			Stack
{
public:
	Stack()
	{
		stack = nullptr;
		i_head = 0;
	}
	void		push(T);
	T			pop();
	void		show();
private:
	T*			stack;
	int			i_head;
};

template <class T>
void			Stack<T>::push(T elem)
{
	T*			temp;

	temp = stack;
	stack = new T[i_head + 1];
	i_head++;
	for (int i = 0; i < i_head - 1; i++)
		stack[i] = temp[i];
	stack[i_head - 1] = elem;
	if (i_head > 1)
		delete	temp;
}

template<class T>
T				Stack<T>::pop()
{
	if(i_head == 0)
		return 0;
	i_head--;
	return stack[i_head];
}

template<class T>
void			Stack<T>::show()
{
	T*			temp;
	temp = stack;
	for (int i = 0; i < i_head; i++)
	{
		cout << *temp << ' ';
		temp++;
	}
	cout << "\n";
}


int				main()
{
	Stack<int> stack;
	stack.push(1);
	stack.push(2);
	stack.show();
	stack.pop();
	stack.show();
}